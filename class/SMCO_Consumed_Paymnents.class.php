<?php
	class SMCO_Consumed_Payments
	{
		public $owners;
		function __construct()
		{
			$this->owners		= array();
		}
		public function pay_summ($owner_id, $summ, $currency_type_id)
		{
			if($summ == 0)	return;
			if(!$this->owners[$owner_id])
				$this->owners[$owner_id]	= array();
			if(!$this->owners[$owner_id][$currency_type_id])
				$this->owners[$owner_id][$currency_type_id] = 0;
			$this->owners[$owner_id][$currency_type_id] += $summ;
		}
		function pay_all($RPT="")
		{
			//echo Assistants::echo_me($this->owners, true);
			foreach($this->owners as $owner_id=>$value)
			{
				$loc_meta	= SMC_Location::get_term_meta($owner_id);
				$def_ct_id	= $loc_meta['currency_type'];
				$loc		= SMC_Location::get_instance($owner_id);
				foreach($value as $key=>$val)
				{
					if($key==0)
					{
						$key	= $def_ct_id;
					}
					$args	= array(
										'numberposts'		=> 1,
										'offset'			=> 0,
										'orderby'  			=> 'id',
										'post_type'			=> SMP_CURRENCY_ACCOUNT,
										'post_status' 		=> 'publish',
										'fields'			=> 'ids',
										'meta_query'		=> array(
											'relation'		=> 'AND',
											array(
												'key'		=> 'ctype_id',
												'value'		=> $key
												 ),
											array(
												'key'		=> 'owner_id',
												'value'		=> $owner_id
												 )
										)
									);
					$ac		= get_posts($args);
					if(!count($ac))	continue;
					//echo "<p>location id= $owner_id, currency type id = $key, summ = $val, account id = ".$ac[0].'</p>';
					SMP_Currency::transfer_to($ac[0], $val, __("System", "smc"), __("Pay from Consume.", "smco"));
					
					$ct		= SMP_Currency_Type::get_instance($key);
					$RPT	.= "<div>". sprintf( __("Pay for %s amount - %s.", "smco"), "<b>".$loc->name."</b>", "<b>".$ct->get_price($val)."</b>" ) ."</div>";
				}
			}
		}
	}
	
?>