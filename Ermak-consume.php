<?php
/*
Plugin Name: Ermak.Consuming
Plugin URI:http://wp-ermak.ru/?page_id=135&d_d=129
Description: Consuming Control for Locations, Users and other stuffs in metagame by SOLING
Version: 1.0.0
Date: 28.08.2015
Author: Genagl
Author http://www.genagl.ru
License: GPL2
*/
/*  Copyright 2014  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//���������� ���������
function init_textdomain_smco() { 
	//if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("smco", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	}
} 
// �������� �� �� ������ ���������� ������� 
add_action('plugins_loaded', 'init_textdomain_smco');


function smco_control_plugins()
{
	global $eb_sticker;
	$arr		= array();
	if(!is_plugin_active('Ermak/Ermak.php'))
	{
		$arr[]	= __('Ermak. Metagame', "smco");
	}
	if(!is_plugin_active('Ermak_production/Ermak_production.php'))
	{
		$arr[]	= __('Ermak Production', "smco");
	}
	if(count($arr)==0)	return true;
	$eb_sticker		= '<div><board_title>'.__("critical gaps plugin functionality Ermak Consume", "smco")."</board_title>";
	$eb_sticker		.= __('Please install the following plugins', "smco").'<ul>';
	foreach($arr as $a)
	{
		$eb_sticker		.= "<li>".$a."</li>";
	}
	$eb_sticker		.= "</ul>".__("Sorry. Ermak Consume not working", "smco")."</div>";
	add_action( 'admin_notices',					'after_install_sticker_smco' , 23);
	return false;
}
function after_install_sticker_smco()
{
	global $eb_sticker;
	echo "
	<div class='updated notice is-dismissible1' id='install_smco_notice' style='padding:30px!important; position: relative;'>
		".
			$eb_sticker .
		"
		<span class='smc_desmiss_button'>
			<span class='screen-reader-text'>".__("Close")."</span>
		</span>
	</div>
	
	";
}
if(!smco_control_plugins())	return;
//Paths
define('SMCo_URLPATH', WP_PLUGIN_URL.'/Ermak_consume/');
define('SMCo','smco_');
define('SMCO_CONSUME_SCHEME_TYPE','smco_consume_scheme');
define('NOTHING_CONSUME_ADD_STATUS',-12);
define('CONSUME_REPORT_TYPE',2);
define('EB_PRODUCTS_CONSUME_PAGE', "eb_consume_slide");
define('SMCo_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
require_once('class/SMCO_Report.php');
require_once('class/SMCO_Assistant.php');
require_once('smco_shortcodes.php');
require_once('class/SMCOclass.php');
require_once('widgets/my_consume_widget.php');
register_activation_hook( __FILE__,  'init_settings'  );

function init_settings()
{
	if( is_plugin_active('Ermak/Ermak.php') )		
	{			
		SMC_Location::add_properties(
										array(
												"consume", 
												"smco_consume_scheme", 
												'user_consume_status',
												'last_user_consume_status',
												'consumer_type',
											  ), 
										array(
												"BIGINT(20) UNSIGNED ", 
												"BIGINT(20) UNSIGNED ", 
												"varchar(255) ",
												"varchar(255) ",
												"varchar(255) "
											  ),
										array(
												0,
												0,
												'',
												'',
												''
											  )
									);			
	}	
	$options												= get_option(SMCo);
	if(!$options)
	{
		$options											= array();
		$options['supported_types']							= array();
		$options['supported_type_names']					= array();
		$options['supported_types']['location']				= true;
		$options['supported_types']['user']					= true;
		$options['supported_type_names']['user'] 	= __("Player", "smc");
	}
	
	$post_types												= get_post_types('','object');
	$taxonomies												= get_taxonomies(array(), "object");
	foreach ($post_types as $post_type ) 
	{
		if(
			$post_type->name == 'nav_menu_item' ||
			$post_type->name == 'revision' ||
			$post_type->name == 'post' ||
			$post_type->name == 'attachment' ||
			$post_type->name == 'goods_type' ||
			$post_type->name == 'smc_currency_type' ||
			$post_type->name == 'location_type' ||
			$post_type->name == SMCO_CONSUME_SCHEME_TYPE
		  )
			continue;
		$options['supported_type_types'][$post_type->name] 	= 'post';
		$options['supported_type_names'][$post_type->name] 	= $post_type->label; 
		//echo $post_type->name . ":" . $_POST[$post_type->name]."<BR>";
	}
	foreach ($taxonomies as $taxonomy ) 
	{
		if(
			$taxonomy->name == 'nav_menu' ||
			$taxonomy->name == 'post_tag' ||
			$taxonomy->name == 'link_category' ||
			$taxonomy->name == 'post_format'
		  )
			continue;
		$options['supported_type_types'][$taxonomy->name] 	= 'taxonomy';
		$options['supported_type_names'][$taxonomy->name] 	= $taxonomy->label; 
	}
	
	$options['consume_interval']							= 20;
	$options['user_consumes_page_ID']						= Ermak_Consume::install();
	update_option(SMCo, $options);
	update_option('install_ermak_consume', 1);
}

	$Ermak_Consume 			= new Ermak_consume();














?>